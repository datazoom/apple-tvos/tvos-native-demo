## Introduction

Datazoom is a high availability real-time data collection solution. This tvOS Project is a demo app that uses DataZoom framework. This document summarizes how to use the demo app.


Version 1.0  : tvOS Demo App

## Running the Native Demo app using Xcode:

* Clone the Project from **[Here](https://gitlab.com/datazoom/apple-tvos/tvos-native-demo.git)**

* Download the framework from **[Here](https://gitlab.com/datazoom/apple-tvos/tvos-collector-libraries-release/blob/master/Native%20Collector/1.8/DZNativeCollector.framework.zip)** and Unzip.
* Replace the DataZoom.framework on the downloaded demo app folder with the framework file downloaded in previous step.

* Open the project (Double click the project file with extension .xcodeproj)

* Select the required Simulator from top left in Xcode
* Click the Play button given on top left corner in Xcode to run the Project



* This will run the project and open the app in selected Simulator
* From simlator, the app can be tested by entering config id.
* You can choose between QA/Dev/Production environment. Make sure that you enter correct config id according to the environment selected.

![Screenshot pane](https://gitlab.com/datazoom/apple-tvos/tvos-native-demo/blob/develop/NativeDemo/Demo%20-%20Apple%20TV%20.png)

* You can observe the events generated and sent to connectors from the log in Xcode or you can check in corresponding connectors.

## Events and Metrics :

* AVPlayer events and metrics are currently captured by the Datazoom tvOS platform.

## Dependencies

* Make sure that you use Xcode Version 9.4.1    

* If you want to use the Framework alone, You can download it from **[Here](https://gitlab.com/datazoom/apple-tvos/tvos-collector-libraries-release/blob/master/Native%20Collector/1.8/DZNativeCollector.framework.zip)** .

* For Instructions on how to use the Framework, You can refer **[Here](https://gitlab.com/datazoom/mobile-tvOS-group/mobile-tvOS-native-collector/blob/develop/README.md)** .   

## Credit

- Vishnu M P

## Link to License/Confidentiality Agreement
Datazoom, Inc ("COMPANY") CONFIDENTIAL
Copyright (c) 2017-2018 [Datazoom, Inc.], All Rights Reserved.
NOTICE:  All information contained herein is, and remains the property of COMPANY. The intellectual and technical concepts contained
herein are proprietary to COMPANY and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
from COMPANY.  Access to the source code contained herein is hereby forbidden to anyone except current COMPANY employees, managers or contractors who have executed
Confidentiality and Non-disclosure agreements explicitly covering such access.
The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
information that is confidential and/or proprietary, and is a trade secret, of  COMPANY.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.






